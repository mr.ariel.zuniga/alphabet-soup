import sys

from enum import Enum

CardinalPoint = Enum('CardinalPoint','W2E E2W N2S S2N NE2SW SW2NE NW2SE SE2NW')

def GetMatrix(matrix):
    return [i.lower().replace(' ','') for i in matrix.split('\n')]

def FindWord(matrix,rows,columns,word):
    for row in range(rows):
        for column in range(columns):
            if matrix[row][column] == word[0].lower():
                for direction in CardinalPoint:
                    r = StartRow = EndRow = row
                    c = StartColumn = EndColumn = column
                    NumberOfMatches = IndexWord = 0
                    match direction:
                        case CardinalPoint.W2E:
                            while c < columns and IndexWord < len(word):
                                if matrix[row][c] == word[IndexWord].lower():
                                    NumberOfMatches += 1
                                    EndColumn = c
                                c += 1
                                IndexWord += 1
                        case CardinalPoint.E2W:
                            while c >= 0 and IndexWord < len(word):
                                if matrix[row][c] == word[IndexWord].lower():
                                    NumberOfMatches += 1
                                    EndColumn = c
                                c -= 1
                                IndexWord += 1
                        case CardinalPoint.N2S:
                            while r < rows and IndexWord < len(word):
                                if matrix[r][column] == word[IndexWord].lower():
                                    NumberOfMatches +=1
                                    EndRow = r
                                r += 1
                                IndexWord += 1
                        case CardinalPoint.S2N:
                            while r >= 0 and IndexWord < len(word):
                                if matrix[r][column] == word[IndexWord].lower():
                                    NumberOfMatches += 1
                                    EndRow = r
                                r -= 1
                                IndexWord += 1
                        case CardinalPoint.NE2SW:
                            while r < rows and c >= 0 and IndexWord < len(word):
                                if matrix[r][c] == word[IndexWord].lower():
                                    NumberOfMatches += 1
                                    EndRow = r
                                    EndColumn = c
                                r += 1
                                c -= 1
                                IndexWord +=1
                        case CardinalPoint.SW2NE:
                            while r >= 0 and c < columns and IndexWord < len(word):
                                if matrix[r][c] == word[IndexWord].lower():
                                    NumberOfMatches += 1
                                    EndRow = r
                                    EndColumn = c
                                r -= 1
                                c += 1
                                IndexWord += 1
                        case CardinalPoint.NW2SE:
                            while r < rows and c < columns and IndexWord < len(word):
                                if matrix[r][c] == word[IndexWord].lower():
                                    NumberOfMatches += 1
                                    EndRow = r
                                    EndColumn = c
                                r += 1
                                c += 1
                                IndexWord +=1
                        case CardinalPoint.SE2NW:
                            while r >= 0 and c >= 0 and IndexWord < len(word):
                                if matrix[r][c] == word[IndexWord].lower():
                                    NumberOfMatches += 1
                                    EndRow = r
                                    EndColumn = c
                                r -= 1
                                c -= 1
                                IndexWord += 1
                    if NumberOfMatches == len(word):
                        print(f"{word} {StartRow}:{StartColumn} {EndRow}:{EndColumn}")
                        return
                
try:
    arg = sys.argv[1]
except IndexError:
    raise SystemExit(f"Use: {sys.argv[0]} InputFile.txt")
try:
    with open(sys.argv[1]) as f:
        r, c = f.readline().strip().split('x')
        m = GetMatrix(f.read(int(r)*int(c)*2))
        for w in f.readlines(): FindWord(m,int(r),int(c),w.strip().replace(' ',''))
except FileNotFoundError:
    raise SystemExit(f"Error: {sys.argv[1]} does not exists")