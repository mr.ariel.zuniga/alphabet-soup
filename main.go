package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

// check command line arguments
func CheckArgs() {
	if len(os.Args) != 2 {
		fmt.Println("Use: main InputFile.txt")
		os.Exit(1)
	}
}

// check if file exists
func CheckFileExists() {
	_, err := os.Stat(os.Args[1])
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Printf("Error: %s does not exists\n", os.Args[1])
			os.Exit(1)
		}
	}
}

// read input file
func ReadInputFile() []string {
	ByteArrayContent, err := os.ReadFile(os.Args[1])
	if err != nil {
		fmt.Println("Error: unable to read file", os.Args[1])
		os.Exit(1)
	}
	StringContent := string(ByteArrayContent)
	Lines := strings.Split(StringContent, "\r\n")
	return Lines
}

// get rows and columns
func GetRowsAndColumns(Line string) (int, int) {
	Token := strings.Split(Line, "x")
	StringRows := Token[0]
	StringColumns := Token[1]
	Rows, err := strconv.Atoi(StringRows)
	if err != nil {
		fmt.Println("Error: unable to convert the number of rows")
		os.Exit(1)
	}
	Columns, err := strconv.Atoi(StringColumns)
	if err != nil {
		fmt.Println("Error: unable to convert the number of columns")
		os.Exit(1)
	}
	return Rows, Columns
}

// parse input file
func ParseInputFile(Lines []string) ([]string, []string, int, int) {
	Rows, Columns := GetRowsAndColumns(Lines[0])
	MatrixWithSpaces := Lines[1 : Rows+1]
	Matrix := []string{}
	for i := 0; i < len(MatrixWithSpaces); i++ {
		NoSpaces := strings.ReplaceAll(MatrixWithSpaces[i], " ", "")
		LowerCase := strings.ToLower(NoSpaces)
		Matrix = append(Matrix, LowerCase)
	}
	WordsWithSpaces := Lines[Rows+1:]
	Words := []string{}
	for i := 0; i < len(WordsWithSpaces); i++ {
		NoSpaces := strings.ReplaceAll(WordsWithSpaces[i], " ", "")
		if len(NoSpaces) == 0 {
			continue
		}
		Words = append(Words, NoSpaces)
	}
	return Matrix, Words, Rows, Columns
}

// find word
func FindWord(Matrix []string, Word string, Rows int, Columns int, Direction string) bool {
	for Row := 0; Row < Rows; Row++ {
		for Column := 0; Column < Columns; Column++ {
			if Matrix[Row][Column] == Word[0] {
				StartRow := Row
				EndRow := Row
				StartColumn := Column
				EndColumn := Column
				NumberOfMatches := 0
				IndexWord := 0
				switch {
				case Direction == "LeftToRight":
					for c := Column; c < Columns && IndexWord < len(Word); c++ {
						if Matrix[Row][c] == Word[IndexWord] {
							NumberOfMatches++
							EndColumn = c
						}
						IndexWord++
					}
				case Direction == "RightToLeft":
					for c := Column; c >= 0 && IndexWord < len(Word); c-- {
						if Matrix[Row][c] == Word[IndexWord] {
							NumberOfMatches++
							EndColumn = c
						}
						IndexWord++
					}
				case Direction == "TopToBottom":
					for r := Row; r < Rows && IndexWord < len(Word); r++ {
						if Matrix[r][Column] == Word[IndexWord] {
							NumberOfMatches++
							EndRow = r
						}
						IndexWord++
					}
				case Direction == "BottomToTop":
					for r := Row; r >= 0 && IndexWord < len(Word); r-- {
						if Matrix[r][Column] == Word[IndexWord] {
							NumberOfMatches++
							EndRow = r
						}
						IndexWord++
					}
				case Direction == "SlashTopToBottom":
					for r, c := Row, Column; r < Rows && c >= 0 && IndexWord < len(Word); r, c = r+1, c-1 {
						if Matrix[r][c] == Word[IndexWord] {
							NumberOfMatches++
							EndRow = r
							EndColumn = c
						}
						IndexWord++
					}
				case Direction == "SlashBottomToTop":
					for r, c := Row, Column; r >= 0 && c < Columns && IndexWord < len(Word); r, c = r-1, c+1 {
						if Matrix[r][c] == Word[IndexWord] {
							NumberOfMatches++
							EndRow = r
							EndColumn = c
						}
						IndexWord++
					}
				case Direction == "BackSlashTopToBottom":
					for r, c := Row, Column; r < Rows && c < Columns && IndexWord < len(Word); r, c = r+1, c+1 {
						if Matrix[r][c] == Word[IndexWord] {
							NumberOfMatches++
							EndRow = r
							EndColumn = c
						}
						IndexWord++
					}
				case Direction == "BackSlashBottomToTop":
					for r, c := Row, Column; r >= 0 && c >= 0 && IndexWord < len(Word); r, c = r-1, c-1 {
						if Matrix[r][c] == Word[IndexWord] {
							NumberOfMatches++
							EndRow = r
							EndColumn = c
						}
						IndexWord++
					}
				}
				if NumberOfMatches == len(Word) {
					fmt.Printf("%d:%d %d:%d\n", StartRow, StartColumn, EndRow, EndColumn)
					return true
				}
			}
		}
	}
	return false
}

// process words
func ProcessWords(Matrix []string, Words []string, Rows int, Columns int) {
	for i := 0; i < len(Words); i++ {
		fmt.Print(Words[i] + " ")
		LowerCase := strings.ToLower(Words[i])
		if FindWord(Matrix, LowerCase, Rows, Columns, "LeftToRight") {
		} else if FindWord(Matrix, LowerCase, Rows, Columns, "RightToLeft") {
		} else if FindWord(Matrix, LowerCase, Rows, Columns, "TopToBottom") {
		} else if FindWord(Matrix, LowerCase, Rows, Columns, "BottomToTop") {
		} else if FindWord(Matrix, LowerCase, Rows, Columns, "SlashTopToBottom") {
		} else if FindWord(Matrix, LowerCase, Rows, Columns, "SlashBottomToTop") {
		} else if FindWord(Matrix, LowerCase, Rows, Columns, "BackSlashTopToBottom") {
		} else if FindWord(Matrix, LowerCase, Rows, Columns, "BackSlashBottomToTop") {
		}
	}
}

// main function
func main() {
	CheckArgs()
	CheckFileExists()
	Lines := ReadInputFile()
	Matrix, Words, Rows, Columns := ParseInputFile(Lines)
	ProcessWords(Matrix, Words, Rows, Columns)
}
